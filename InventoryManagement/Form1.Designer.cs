﻿namespace InventoryManagement
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.buttonUpdateOne = new System.Windows.Forms.Button();
            this.buttonQuantReset = new System.Windows.Forms.Button();
            this.buttonUpdatePlus = new System.Windows.Forms.Button();
            this.buttonQuantPlus = new System.Windows.Forms.Button();
            this.buttonPriceMinus = new System.Windows.Forms.Button();
            this.buttonChangeYearToMinusPlus = new System.Windows.Forms.Button();
            this.buttonChangeYearFromMinusPlus = new System.Windows.Forms.Button();
            this.buttonChangeYearToMinus = new System.Windows.Forms.Button();
            this.buttonChangeYearFromMinus = new System.Windows.Forms.Button();
            this.buttonConditionChange = new System.Windows.Forms.Button();
            this.buttonPricePlus10 = new System.Windows.Forms.Button();
            this.buttonPriceMinus10 = new System.Windows.Forms.Button();
            this.buttonUpdateMinus = new System.Windows.Forms.Button();
            this.buttonPricePlus = new System.Windows.Forms.Button();
            this.buttonQuantMinus = new System.Windows.Forms.Button();
            this.textBoxQuantity = new System.Windows.Forms.TextBox();
            this.πΡΟΙΟΝΤΑBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._InventoryProgram_0_1DataSet = new InventoryManagement._InventoryProgram_0_1DataSet();
            this.textBoxAutoCode = new System.Windows.Forms.TextBox();
            this.textBoxupdate_interval = new System.Windows.Forms.TextBox();
            this.comboBoxCategory = new System.Windows.Forms.ComboBox();
            this.categoryidBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.uniqueidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.makeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yearfromDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yeartoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.photoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conditionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.updateintervalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.κΑΤΗΓΟΡΙΑDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.μΑΡΚΑDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.μΟΝΤΕΛΟDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.πΟΣΟΤΗΤΑDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weightkgDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salePriceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoriesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tagsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPhoto = new System.Windows.Forms.TextBox();
            this.buttonAddImage = new System.Windows.Forms.Button();
            this.comboBoxCondition = new System.Windows.Forms.ComboBox();
            this.conditionBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.comboBoxYearTo = new System.Windows.Forms.ComboBox();
            this.yearsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.comboBoxYearFrom = new System.Windows.Forms.ComboBox();
            this.yearsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboBoxModel = new System.Windows.Forms.ComboBox();
            this.modelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboBoxMake = new System.Windows.Forms.ComboBox();
            this.makeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPREVIOUS = new System.Windows.Forms.Button();
            this.buttonSAVE = new System.Windows.Forms.Button();
            this.buttonexport = new System.Windows.Forms.Button();
            this.buttonDELETE = new System.Windows.Forms.Button();
            this.buttonclose = new System.Windows.Forms.Button();
            this.buttonADD = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.txtdes = new System.Windows.Forms.TextBox();
            this.txttitle = new System.Windows.Forms.TextBox();
            this.txtcode = new System.Windows.Forms.TextBox();
            this.buttonDownload = new System.Windows.Forms.Button();
            this.buttonUpload = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBoxDataBase = new System.Windows.Forms.GroupBox();
            this.buttonResetDatabaseBackups = new System.Windows.Forms.Button();
            this.comboBoxDatabaseBackups = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.conditionΠΡΟΙΟΝΤΑBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.conditionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.makeBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.πΡΟΙΟΝΤΑTableAdapter = new InventoryManagement._InventoryProgram_0_1DataSetTableAdapters.ΠΡΟΙΟΝΤΑTableAdapter();
            this.makeTableAdapter = new InventoryManagement._InventoryProgram_0_1DataSetTableAdapters.makeTableAdapter();
            this.modelTableAdapter = new InventoryManagement._InventoryProgram_0_1DataSetTableAdapters.modelTableAdapter();
            this.πΡΟΙΟΝΤΑBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.πΡΟΙΟΝΤΑBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.yearsTableAdapter = new InventoryManagement._InventoryProgram_0_1DataSetTableAdapters.yearsTableAdapter();
            this.category_idTableAdapter = new InventoryManagement._InventoryProgram_0_1DataSetTableAdapters.category_idTableAdapter();
            this.conditionTableAdapter = new InventoryManagement._InventoryProgram_0_1DataSetTableAdapters.conditionTableAdapter();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.πΡΟΙΟΝΤΑBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._InventoryProgram_0_1DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryidBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conditionBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.makeBindingSource)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBoxDataBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.conditionΠΡΟΙΟΝΤΑBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conditionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.makeBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.πΡΟΙΟΝΤΑBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.πΡΟΙΟΝΤΑBindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.DarkGray;
            this.tabPage3.Controls.Add(this.buttonUpdateOne);
            this.tabPage3.Controls.Add(this.buttonQuantReset);
            this.tabPage3.Controls.Add(this.buttonUpdatePlus);
            this.tabPage3.Controls.Add(this.buttonQuantPlus);
            this.tabPage3.Controls.Add(this.buttonPriceMinus);
            this.tabPage3.Controls.Add(this.buttonChangeYearToMinusPlus);
            this.tabPage3.Controls.Add(this.buttonChangeYearFromMinusPlus);
            this.tabPage3.Controls.Add(this.buttonChangeYearToMinus);
            this.tabPage3.Controls.Add(this.buttonChangeYearFromMinus);
            this.tabPage3.Controls.Add(this.buttonConditionChange);
            this.tabPage3.Controls.Add(this.buttonPricePlus10);
            this.tabPage3.Controls.Add(this.buttonPriceMinus10);
            this.tabPage3.Controls.Add(this.buttonUpdateMinus);
            this.tabPage3.Controls.Add(this.buttonPricePlus);
            this.tabPage3.Controls.Add(this.buttonQuantMinus);
            this.tabPage3.Controls.Add(this.textBoxQuantity);
            this.tabPage3.Controls.Add(this.textBoxAutoCode);
            this.tabPage3.Controls.Add(this.textBoxupdate_interval);
            this.tabPage3.Controls.Add(this.comboBoxCategory);
            this.tabPage3.Controls.Add(this.dataGridView1);
            this.tabPage3.Controls.Add(this.txtPhoto);
            this.tabPage3.Controls.Add(this.buttonAddImage);
            this.tabPage3.Controls.Add(this.comboBoxCondition);
            this.tabPage3.Controls.Add(this.comboBoxYearTo);
            this.tabPage3.Controls.Add(this.comboBoxYearFrom);
            this.tabPage3.Controls.Add(this.comboBoxModel);
            this.tabPage3.Controls.Add(this.comboBoxMake);
            this.tabPage3.Controls.Add(this.buttonNext);
            this.tabPage3.Controls.Add(this.buttonPREVIOUS);
            this.tabPage3.Controls.Add(this.buttonSAVE);
            this.tabPage3.Controls.Add(this.buttonexport);
            this.tabPage3.Controls.Add(this.buttonDELETE);
            this.tabPage3.Controls.Add(this.buttonclose);
            this.tabPage3.Controls.Add(this.buttonADD);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.txtprice);
            this.tabPage3.Controls.Add(this.txtdes);
            this.tabPage3.Controls.Add(this.txttitle);
            this.tabPage3.Controls.Add(this.txtcode);
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1646, 979);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ΕΙΣΑΓΩΓΗ ΠΡΟΙΟΝΤΩΝ";
            // 
            // buttonUpdateOne
            // 
            this.buttonUpdateOne.Location = new System.Drawing.Point(387, 196);
            this.buttonUpdateOne.Name = "buttonUpdateOne";
            this.buttonUpdateOne.Size = new System.Drawing.Size(45, 31);
            this.buttonUpdateOne.TabIndex = 27;
            this.buttonUpdateOne.Text = "1";
            this.buttonUpdateOne.UseVisualStyleBackColor = true;
            this.buttonUpdateOne.Click += new System.EventHandler(this.buttonUpdateOne_Click);
            // 
            // buttonQuantReset
            // 
            this.buttonQuantReset.Location = new System.Drawing.Point(387, 233);
            this.buttonQuantReset.Name = "buttonQuantReset";
            this.buttonQuantReset.Size = new System.Drawing.Size(45, 31);
            this.buttonQuantReset.TabIndex = 27;
            this.buttonQuantReset.Text = "0";
            this.buttonQuantReset.UseVisualStyleBackColor = true;
            this.buttonQuantReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonUpdatePlus
            // 
            this.buttonUpdatePlus.Location = new System.Drawing.Point(336, 196);
            this.buttonUpdatePlus.Name = "buttonUpdatePlus";
            this.buttonUpdatePlus.Size = new System.Drawing.Size(45, 31);
            this.buttonUpdatePlus.TabIndex = 27;
            this.buttonUpdatePlus.Text = "+";
            this.buttonUpdatePlus.UseVisualStyleBackColor = true;
            this.buttonUpdatePlus.Click += new System.EventHandler(this.buttonUpdatePlus_Click);
            // 
            // buttonQuantPlus
            // 
            this.buttonQuantPlus.Location = new System.Drawing.Point(336, 233);
            this.buttonQuantPlus.Name = "buttonQuantPlus";
            this.buttonQuantPlus.Size = new System.Drawing.Size(45, 31);
            this.buttonQuantPlus.TabIndex = 27;
            this.buttonQuantPlus.Text = "+";
            this.buttonQuantPlus.UseVisualStyleBackColor = true;
            this.buttonQuantPlus.Click += new System.EventHandler(this.buttonPlus_Click);
            // 
            // buttonPriceMinus
            // 
            this.buttonPriceMinus.Location = new System.Drawing.Point(709, 6);
            this.buttonPriceMinus.Name = "buttonPriceMinus";
            this.buttonPriceMinus.Size = new System.Drawing.Size(36, 31);
            this.buttonPriceMinus.TabIndex = 27;
            this.buttonPriceMinus.Text = "-";
            this.buttonPriceMinus.UseVisualStyleBackColor = true;
            this.buttonPriceMinus.Click += new System.EventHandler(this.buttonPriceMinus_Click);
            // 
            // buttonChangeYearToMinusPlus
            // 
            this.buttonChangeYearToMinusPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.buttonChangeYearToMinusPlus.Location = new System.Drawing.Point(849, 126);
            this.buttonChangeYearToMinusPlus.Name = "buttonChangeYearToMinusPlus";
            this.buttonChangeYearToMinusPlus.Size = new System.Drawing.Size(22, 18);
            this.buttonChangeYearToMinusPlus.TabIndex = 27;
            this.buttonChangeYearToMinusPlus.Text = "+";
            this.buttonChangeYearToMinusPlus.UseVisualStyleBackColor = true;
            this.buttonChangeYearToMinusPlus.Click += new System.EventHandler(this.buttonChangeYearToMinusPlus_Click);
            // 
            // buttonChangeYearFromMinusPlus
            // 
            this.buttonChangeYearFromMinusPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.buttonChangeYearFromMinusPlus.Location = new System.Drawing.Point(849, 89);
            this.buttonChangeYearFromMinusPlus.Name = "buttonChangeYearFromMinusPlus";
            this.buttonChangeYearFromMinusPlus.Size = new System.Drawing.Size(22, 18);
            this.buttonChangeYearFromMinusPlus.TabIndex = 27;
            this.buttonChangeYearFromMinusPlus.Text = "+";
            this.buttonChangeYearFromMinusPlus.UseVisualStyleBackColor = true;
            this.buttonChangeYearFromMinusPlus.Click += new System.EventHandler(this.buttonChangeYearFromMinusPlus_Click);
            // 
            // buttonChangeYearToMinus
            // 
            this.buttonChangeYearToMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.buttonChangeYearToMinus.Location = new System.Drawing.Point(821, 126);
            this.buttonChangeYearToMinus.Name = "buttonChangeYearToMinus";
            this.buttonChangeYearToMinus.Size = new System.Drawing.Size(22, 18);
            this.buttonChangeYearToMinus.TabIndex = 27;
            this.buttonChangeYearToMinus.Text = "-";
            this.buttonChangeYearToMinus.UseVisualStyleBackColor = true;
            this.buttonChangeYearToMinus.Click += new System.EventHandler(this.buttonChangeYearToMinus_Click);
            // 
            // buttonChangeYearFromMinus
            // 
            this.buttonChangeYearFromMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.buttonChangeYearFromMinus.Location = new System.Drawing.Point(821, 89);
            this.buttonChangeYearFromMinus.Name = "buttonChangeYearFromMinus";
            this.buttonChangeYearFromMinus.Size = new System.Drawing.Size(22, 18);
            this.buttonChangeYearFromMinus.TabIndex = 27;
            this.buttonChangeYearFromMinus.Text = "-";
            this.buttonChangeYearFromMinus.UseVisualStyleBackColor = true;
            this.buttonChangeYearFromMinus.Click += new System.EventHandler(this.buttonChangeYearFromMinus_Click);
            // 
            // buttonConditionChange
            // 
            this.buttonConditionChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.buttonConditionChange.Location = new System.Drawing.Point(821, 43);
            this.buttonConditionChange.Name = "buttonConditionChange";
            this.buttonConditionChange.Size = new System.Drawing.Size(50, 31);
            this.buttonConditionChange.TabIndex = 27;
            this.buttonConditionChange.Text = "<  >";
            this.buttonConditionChange.UseVisualStyleBackColor = true;
            this.buttonConditionChange.Click += new System.EventHandler(this.buttonConditionChange_Click);
            // 
            // buttonPricePlus10
            // 
            this.buttonPricePlus10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.buttonPricePlus10.Location = new System.Drawing.Point(835, 6);
            this.buttonPricePlus10.Name = "buttonPricePlus10";
            this.buttonPricePlus10.Size = new System.Drawing.Size(36, 31);
            this.buttonPricePlus10.TabIndex = 27;
            this.buttonPricePlus10.Text = "+10";
            this.buttonPricePlus10.UseVisualStyleBackColor = true;
            this.buttonPricePlus10.Click += new System.EventHandler(this.buttonPricePlus10_Click);
            // 
            // buttonPriceMinus10
            // 
            this.buttonPriceMinus10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.buttonPriceMinus10.Location = new System.Drawing.Point(793, 6);
            this.buttonPriceMinus10.Name = "buttonPriceMinus10";
            this.buttonPriceMinus10.Size = new System.Drawing.Size(36, 31);
            this.buttonPriceMinus10.TabIndex = 27;
            this.buttonPriceMinus10.Text = "-10";
            this.buttonPriceMinus10.UseVisualStyleBackColor = true;
            this.buttonPriceMinus10.Click += new System.EventHandler(this.buttonPriceMinus10_Click);
            // 
            // buttonUpdateMinus
            // 
            this.buttonUpdateMinus.Location = new System.Drawing.Point(285, 197);
            this.buttonUpdateMinus.Name = "buttonUpdateMinus";
            this.buttonUpdateMinus.Size = new System.Drawing.Size(45, 31);
            this.buttonUpdateMinus.TabIndex = 27;
            this.buttonUpdateMinus.Text = "-";
            this.buttonUpdateMinus.UseVisualStyleBackColor = true;
            this.buttonUpdateMinus.Click += new System.EventHandler(this.buttonUpdateMinus_Click);
            // 
            // buttonPricePlus
            // 
            this.buttonPricePlus.Location = new System.Drawing.Point(751, 6);
            this.buttonPricePlus.Name = "buttonPricePlus";
            this.buttonPricePlus.Size = new System.Drawing.Size(36, 31);
            this.buttonPricePlus.TabIndex = 27;
            this.buttonPricePlus.Text = "+";
            this.buttonPricePlus.UseVisualStyleBackColor = true;
            this.buttonPricePlus.Click += new System.EventHandler(this.buttonPricePlus_Click);
            // 
            // buttonQuantMinus
            // 
            this.buttonQuantMinus.Location = new System.Drawing.Point(285, 234);
            this.buttonQuantMinus.Name = "buttonQuantMinus";
            this.buttonQuantMinus.Size = new System.Drawing.Size(45, 31);
            this.buttonQuantMinus.TabIndex = 27;
            this.buttonQuantMinus.Text = "-";
            this.buttonQuantMinus.UseVisualStyleBackColor = true;
            this.buttonQuantMinus.Click += new System.EventHandler(this.buttonMinus_Click);
            // 
            // textBoxQuantity
            // 
            this.textBoxQuantity.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.πΡΟΙΟΝΤΑBindingSource, "ΠΟΣΟΤΗΤΑ", true));
            this.textBoxQuantity.Location = new System.Drawing.Point(155, 234);
            this.textBoxQuantity.Name = "textBoxQuantity";
            this.textBoxQuantity.Size = new System.Drawing.Size(124, 31);
            this.textBoxQuantity.TabIndex = 26;
            // 
            // πΡΟΙΟΝΤΑBindingSource
            // 
            this.πΡΟΙΟΝΤΑBindingSource.DataMember = "ΠΡΟΙΟΝΤΑ";
            this.πΡΟΙΟΝΤΑBindingSource.DataSource = this._InventoryProgram_0_1DataSet;
            // 
            // _InventoryProgram_0_1DataSet
            // 
            this._InventoryProgram_0_1DataSet.DataSetName = "_InventoryProgram_0_1DataSet";
            this._InventoryProgram_0_1DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // textBoxAutoCode
            // 
            this.textBoxAutoCode.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.πΡΟΙΟΝΤΑBindingSource, "smart_code", true));
            this.textBoxAutoCode.Location = new System.Drawing.Point(587, 160);
            this.textBoxAutoCode.Name = "textBoxAutoCode";
            this.textBoxAutoCode.ReadOnly = true;
            this.textBoxAutoCode.Size = new System.Drawing.Size(284, 31);
            this.textBoxAutoCode.TabIndex = 25;
            // 
            // textBoxupdate_interval
            // 
            this.textBoxupdate_interval.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.πΡΟΙΟΝΤΑBindingSource, "update_interval", true));
            this.textBoxupdate_interval.Location = new System.Drawing.Point(184, 196);
            this.textBoxupdate_interval.Name = "textBoxupdate_interval";
            this.textBoxupdate_interval.Size = new System.Drawing.Size(95, 31);
            this.textBoxupdate_interval.TabIndex = 24;
            // 
            // comboBoxCategory
            // 
            this.comboBoxCategory.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.πΡΟΙΟΝΤΑBindingSource, "category_id", true));
            this.comboBoxCategory.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.πΡΟΙΟΝΤΑBindingSource, "ΚΑΤΗΓΟΡΙΑ", true));
            this.comboBoxCategory.DataSource = this.categoryidBindingSource;
            this.comboBoxCategory.DisplayMember = "name";
            this.comboBoxCategory.FormattingEnabled = true;
            this.comboBoxCategory.Location = new System.Drawing.Point(155, 80);
            this.comboBoxCategory.Name = "comboBoxCategory";
            this.comboBoxCategory.Size = new System.Drawing.Size(277, 33);
            this.comboBoxCategory.TabIndex = 23;
            this.comboBoxCategory.ValueMember = "id";
            this.comboBoxCategory.Click += new System.EventHandler(this.comboBox2_Click);
            // 
            // categoryidBindingSource
            // 
            this.categoryidBindingSource.DataMember = "category_id";
            this.categoryidBindingSource.DataSource = this._InventoryProgram_0_1DataSet;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.uniqueidDataGridViewTextBoxColumn,
            this.titleDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn,
            this.categoryidDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.makeDataGridViewTextBoxColumn,
            this.modelDataGridViewTextBoxColumn,
            this.yearfromDataGridViewTextBoxColumn,
            this.yeartoDataGridViewTextBoxColumn,
            this.photoDataGridViewTextBoxColumn,
            this.conditionDataGridViewTextBoxColumn,
            this.updateintervalDataGridViewTextBoxColumn,
            this.κΑΤΗΓΟΡΙΑDataGridViewTextBoxColumn,
            this.μΑΡΚΑDataGridViewTextBoxColumn,
            this.μΟΝΤΕΛΟDataGridViewTextBoxColumn,
            this.πΟΣΟΤΗΤΑDataGridViewTextBoxColumn,
            this.weightkgDataGridViewTextBoxColumn,
            this.salePriceDataGridViewTextBoxColumn,
            this.categoriesDataGridViewTextBoxColumn,
            this.tagsDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.πΡΟΙΟΝΤΑBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(20, 351);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Size = new System.Drawing.Size(1620, 610);
            this.dataGridView1.TabIndex = 22;
            // 
            // uniqueidDataGridViewTextBoxColumn
            // 
            this.uniqueidDataGridViewTextBoxColumn.DataPropertyName = "unique_id";
            this.uniqueidDataGridViewTextBoxColumn.HeaderText = "unique_id";
            this.uniqueidDataGridViewTextBoxColumn.Name = "uniqueidDataGridViewTextBoxColumn";
            this.uniqueidDataGridViewTextBoxColumn.Width = 140;
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "title";
            this.titleDataGridViewTextBoxColumn.HeaderText = "title";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            this.titleDataGridViewTextBoxColumn.Width = 76;
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            this.descriptionDataGridViewTextBoxColumn.Width = 153;
            // 
            // categoryidDataGridViewTextBoxColumn
            // 
            this.categoryidDataGridViewTextBoxColumn.DataPropertyName = "category_id";
            this.categoryidDataGridViewTextBoxColumn.HeaderText = "category_id";
            this.categoryidDataGridViewTextBoxColumn.Name = "categoryidDataGridViewTextBoxColumn";
            this.categoryidDataGridViewTextBoxColumn.Width = 160;
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "price";
            this.priceDataGridViewTextBoxColumn.HeaderText = "price";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.Width = 89;
            // 
            // makeDataGridViewTextBoxColumn
            // 
            this.makeDataGridViewTextBoxColumn.DataPropertyName = "make";
            this.makeDataGridViewTextBoxColumn.HeaderText = "make";
            this.makeDataGridViewTextBoxColumn.Name = "makeDataGridViewTextBoxColumn";
            this.makeDataGridViewTextBoxColumn.Width = 93;
            // 
            // modelDataGridViewTextBoxColumn
            // 
            this.modelDataGridViewTextBoxColumn.DataPropertyName = "model";
            this.modelDataGridViewTextBoxColumn.HeaderText = "model";
            this.modelDataGridViewTextBoxColumn.Name = "modelDataGridViewTextBoxColumn";
            // 
            // yearfromDataGridViewTextBoxColumn
            // 
            this.yearfromDataGridViewTextBoxColumn.DataPropertyName = "yearfrom";
            this.yearfromDataGridViewTextBoxColumn.HeaderText = "yearfrom";
            this.yearfromDataGridViewTextBoxColumn.Name = "yearfromDataGridViewTextBoxColumn";
            this.yearfromDataGridViewTextBoxColumn.Width = 129;
            // 
            // yeartoDataGridViewTextBoxColumn
            // 
            this.yeartoDataGridViewTextBoxColumn.DataPropertyName = "yearto";
            this.yeartoDataGridViewTextBoxColumn.HeaderText = "yearto";
            this.yeartoDataGridViewTextBoxColumn.Name = "yeartoDataGridViewTextBoxColumn";
            this.yeartoDataGridViewTextBoxColumn.Width = 103;
            // 
            // photoDataGridViewTextBoxColumn
            // 
            this.photoDataGridViewTextBoxColumn.DataPropertyName = "photo";
            this.photoDataGridViewTextBoxColumn.HeaderText = "photo";
            this.photoDataGridViewTextBoxColumn.Name = "photoDataGridViewTextBoxColumn";
            this.photoDataGridViewTextBoxColumn.Width = 96;
            // 
            // conditionDataGridViewTextBoxColumn
            // 
            this.conditionDataGridViewTextBoxColumn.DataPropertyName = "condition";
            this.conditionDataGridViewTextBoxColumn.HeaderText = "condition";
            this.conditionDataGridViewTextBoxColumn.Name = "conditionDataGridViewTextBoxColumn";
            this.conditionDataGridViewTextBoxColumn.Width = 133;
            // 
            // updateintervalDataGridViewTextBoxColumn
            // 
            this.updateintervalDataGridViewTextBoxColumn.DataPropertyName = "update_interval";
            this.updateintervalDataGridViewTextBoxColumn.HeaderText = "update_interval";
            this.updateintervalDataGridViewTextBoxColumn.Name = "updateintervalDataGridViewTextBoxColumn";
            this.updateintervalDataGridViewTextBoxColumn.Width = 200;
            // 
            // κΑΤΗΓΟΡΙΑDataGridViewTextBoxColumn
            // 
            this.κΑΤΗΓΟΡΙΑDataGridViewTextBoxColumn.DataPropertyName = "ΚΑΤΗΓΟΡΙΑ";
            this.κΑΤΗΓΟΡΙΑDataGridViewTextBoxColumn.HeaderText = "ΚΑΤΗΓΟΡΙΑ";
            this.κΑΤΗΓΟΡΙΑDataGridViewTextBoxColumn.Name = "κΑΤΗΓΟΡΙΑDataGridViewTextBoxColumn";
            this.κΑΤΗΓΟΡΙΑDataGridViewTextBoxColumn.Width = 163;
            // 
            // μΑΡΚΑDataGridViewTextBoxColumn
            // 
            this.μΑΡΚΑDataGridViewTextBoxColumn.DataPropertyName = "ΜΑΡΚΑ";
            this.μΑΡΚΑDataGridViewTextBoxColumn.HeaderText = "ΜΑΡΚΑ";
            this.μΑΡΚΑDataGridViewTextBoxColumn.Name = "μΑΡΚΑDataGridViewTextBoxColumn";
            this.μΑΡΚΑDataGridViewTextBoxColumn.Width = 116;
            // 
            // μΟΝΤΕΛΟDataGridViewTextBoxColumn
            // 
            this.μΟΝΤΕΛΟDataGridViewTextBoxColumn.DataPropertyName = "ΜΟΝΤΕΛΟ";
            this.μΟΝΤΕΛΟDataGridViewTextBoxColumn.HeaderText = "ΜΟΝΤΕΛΟ";
            this.μΟΝΤΕΛΟDataGridViewTextBoxColumn.Name = "μΟΝΤΕΛΟDataGridViewTextBoxColumn";
            this.μΟΝΤΕΛΟDataGridViewTextBoxColumn.Width = 150;
            // 
            // πΟΣΟΤΗΤΑDataGridViewTextBoxColumn
            // 
            this.πΟΣΟΤΗΤΑDataGridViewTextBoxColumn.DataPropertyName = "ΠΟΣΟΤΗΤΑ";
            this.πΟΣΟΤΗΤΑDataGridViewTextBoxColumn.HeaderText = "ΠΟΣΟΤΗΤΑ";
            this.πΟΣΟΤΗΤΑDataGridViewTextBoxColumn.Name = "πΟΣΟΤΗΤΑDataGridViewTextBoxColumn";
            this.πΟΣΟΤΗΤΑDataGridViewTextBoxColumn.Width = 160;
            // 
            // weightkgDataGridViewTextBoxColumn
            // 
            this.weightkgDataGridViewTextBoxColumn.DataPropertyName = "\"Weight (kg)\"";
            this.weightkgDataGridViewTextBoxColumn.HeaderText = "\"Weight (kg)\"";
            this.weightkgDataGridViewTextBoxColumn.Name = "weightkgDataGridViewTextBoxColumn";
            this.weightkgDataGridViewTextBoxColumn.Width = 174;
            // 
            // salePriceDataGridViewTextBoxColumn
            // 
            this.salePriceDataGridViewTextBoxColumn.DataPropertyName = "\"Sale price\"";
            this.salePriceDataGridViewTextBoxColumn.HeaderText = "\"Sale price\"";
            this.salePriceDataGridViewTextBoxColumn.Name = "salePriceDataGridViewTextBoxColumn";
            this.salePriceDataGridViewTextBoxColumn.Width = 159;
            // 
            // categoriesDataGridViewTextBoxColumn
            // 
            this.categoriesDataGridViewTextBoxColumn.DataPropertyName = "Categories";
            this.categoriesDataGridViewTextBoxColumn.HeaderText = "Categories";
            this.categoriesDataGridViewTextBoxColumn.Name = "categoriesDataGridViewTextBoxColumn";
            this.categoriesDataGridViewTextBoxColumn.Width = 151;
            // 
            // tagsDataGridViewTextBoxColumn
            // 
            this.tagsDataGridViewTextBoxColumn.DataPropertyName = "Tags";
            this.tagsDataGridViewTextBoxColumn.HeaderText = "Tags";
            this.tagsDataGridViewTextBoxColumn.Name = "tagsDataGridViewTextBoxColumn";
            this.tagsDataGridViewTextBoxColumn.Width = 89;
            // 
            // txtPhoto
            // 
            this.txtPhoto.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.πΡΟΙΟΝΤΑBindingSource, "photo", true));
            this.txtPhoto.Location = new System.Drawing.Point(587, 196);
            this.txtPhoto.Name = "txtPhoto";
            this.txtPhoto.ReadOnly = true;
            this.txtPhoto.Size = new System.Drawing.Size(693, 31);
            this.txtPhoto.TabIndex = 20;
            // 
            // buttonAddImage
            // 
            this.buttonAddImage.Location = new System.Drawing.Point(1286, 196);
            this.buttonAddImage.Name = "buttonAddImage";
            this.buttonAddImage.Size = new System.Drawing.Size(164, 31);
            this.buttonAddImage.TabIndex = 19;
            this.buttonAddImage.Text = "ADD IMAGE";
            this.buttonAddImage.UseVisualStyleBackColor = true;
            this.buttonAddImage.Click += new System.EventHandler(this.buttonAddImage_Click);
            // 
            // comboBoxCondition
            // 
            this.comboBoxCondition.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.πΡΟΙΟΝΤΑBindingSource, "condition", true));
            this.comboBoxCondition.DataSource = this.conditionBindingSource1;
            this.comboBoxCondition.DisplayMember = "condition";
            this.comboBoxCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCondition.FormattingEnabled = true;
            this.comboBoxCondition.Location = new System.Drawing.Point(587, 43);
            this.comboBoxCondition.Name = "comboBoxCondition";
            this.comboBoxCondition.Size = new System.Drawing.Size(228, 33);
            this.comboBoxCondition.TabIndex = 18;
            this.comboBoxCondition.ValueMember = "condition";
            // 
            // conditionBindingSource1
            // 
            this.conditionBindingSource1.DataMember = "condition";
            this.conditionBindingSource1.DataSource = this._InventoryProgram_0_1DataSet;
            // 
            // comboBoxYearTo
            // 
            this.comboBoxYearTo.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.πΡΟΙΟΝΤΑBindingSource, "yearto", true));
            this.comboBoxYearTo.DataSource = this.yearsBindingSource1;
            this.comboBoxYearTo.DisplayMember = "ID";
            this.comboBoxYearTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxYearTo.FormattingEnabled = true;
            this.comboBoxYearTo.Location = new System.Drawing.Point(587, 121);
            this.comboBoxYearTo.Name = "comboBoxYearTo";
            this.comboBoxYearTo.Size = new System.Drawing.Size(228, 33);
            this.comboBoxYearTo.TabIndex = 7;
            this.comboBoxYearTo.ValueMember = "ID";
            // 
            // yearsBindingSource1
            // 
            this.yearsBindingSource1.DataMember = "years";
            this.yearsBindingSource1.DataSource = this._InventoryProgram_0_1DataSet;
            // 
            // comboBoxYearFrom
            // 
            this.comboBoxYearFrom.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.πΡΟΙΟΝΤΑBindingSource, "yearfrom", true));
            this.comboBoxYearFrom.DataSource = this.yearsBindingSource;
            this.comboBoxYearFrom.DisplayMember = "ID";
            this.comboBoxYearFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxYearFrom.FormattingEnabled = true;
            this.comboBoxYearFrom.Location = new System.Drawing.Point(587, 82);
            this.comboBoxYearFrom.Name = "comboBoxYearFrom";
            this.comboBoxYearFrom.Size = new System.Drawing.Size(228, 33);
            this.comboBoxYearFrom.TabIndex = 6;
            this.comboBoxYearFrom.ValueMember = "ID";
            // 
            // yearsBindingSource
            // 
            this.yearsBindingSource.DataMember = "years";
            this.yearsBindingSource.DataSource = this._InventoryProgram_0_1DataSet;
            // 
            // comboBoxModel
            // 
            this.comboBoxModel.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.πΡΟΙΟΝΤΑBindingSource, "model", true));
            this.comboBoxModel.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.πΡΟΙΟΝΤΑBindingSource, "ΜΟΝΤΕΛΟ", true));
            this.comboBoxModel.DataSource = this.modelBindingSource;
            this.comboBoxModel.DisplayMember = "model_name";
            this.comboBoxModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxModel.FormattingEnabled = true;
            this.comboBoxModel.Location = new System.Drawing.Point(155, 156);
            this.comboBoxModel.Name = "comboBoxModel";
            this.comboBoxModel.Size = new System.Drawing.Size(277, 33);
            this.comboBoxModel.TabIndex = 5;
            this.comboBoxModel.ValueMember = "model_id";
            // 
            // modelBindingSource
            // 
            this.modelBindingSource.DataMember = "model";
            this.modelBindingSource.DataSource = this._InventoryProgram_0_1DataSet;
            // 
            // comboBoxMake
            // 
            this.comboBoxMake.Cursor = System.Windows.Forms.Cursors.Hand;
            this.comboBoxMake.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.πΡΟΙΟΝΤΑBindingSource, "make", true));
            this.comboBoxMake.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.πΡΟΙΟΝΤΑBindingSource, "ΜΑΡΚΑ", true));
            this.comboBoxMake.DataSource = this.makeBindingSource;
            this.comboBoxMake.DisplayMember = "name";
            this.comboBoxMake.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMake.FormattingEnabled = true;
            this.comboBoxMake.Location = new System.Drawing.Point(155, 117);
            this.comboBoxMake.Name = "comboBoxMake";
            this.comboBoxMake.Size = new System.Drawing.Size(277, 33);
            this.comboBoxMake.TabIndex = 4;
            this.comboBoxMake.ValueMember = "make_id";
            this.comboBoxMake.SelectedIndexChanged += new System.EventHandler(this.comboBoxMake_SelectedIndexChanged);
            // 
            // makeBindingSource
            // 
            this.makeBindingSource.DataMember = "make";
            this.makeBindingSource.DataSource = this._InventoryProgram_0_1DataSet;
            // 
            // buttonNext
            // 
            this.buttonNext.Location = new System.Drawing.Point(1285, 314);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(165, 31);
            this.buttonNext.TabIndex = 12;
            this.buttonNext.Text = "NEXT";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonPREVIOUS
            // 
            this.buttonPREVIOUS.Location = new System.Drawing.Point(1456, 314);
            this.buttonPREVIOUS.Name = "buttonPREVIOUS";
            this.buttonPREVIOUS.Size = new System.Drawing.Size(165, 31);
            this.buttonPREVIOUS.TabIndex = 11;
            this.buttonPREVIOUS.Text = "PREVIOUS";
            this.buttonPREVIOUS.UseVisualStyleBackColor = true;
            this.buttonPREVIOUS.Click += new System.EventHandler(this.buttonPREVIOUS_Click);
            // 
            // buttonSAVE
            // 
            this.buttonSAVE.Location = new System.Drawing.Point(1475, 191);
            this.buttonSAVE.Name = "buttonSAVE";
            this.buttonSAVE.Size = new System.Drawing.Size(165, 31);
            this.buttonSAVE.TabIndex = 13;
            this.buttonSAVE.Text = "SAVE";
            this.buttonSAVE.UseVisualStyleBackColor = true;
            this.buttonSAVE.Click += new System.EventHandler(this.buttonSAVE_Click);
            // 
            // buttonexport
            // 
            this.buttonexport.Location = new System.Drawing.Point(1475, 63);
            this.buttonexport.Name = "buttonexport";
            this.buttonexport.Size = new System.Drawing.Size(165, 31);
            this.buttonexport.TabIndex = 16;
            this.buttonexport.Text = "EXPORT";
            this.buttonexport.UseVisualStyleBackColor = true;
            this.buttonexport.Click += new System.EventHandler(this.buttonexport_Click);
            // 
            // buttonDELETE
            // 
            this.buttonDELETE.Location = new System.Drawing.Point(1475, 100);
            this.buttonDELETE.Name = "buttonDELETE";
            this.buttonDELETE.Size = new System.Drawing.Size(165, 31);
            this.buttonDELETE.TabIndex = 14;
            this.buttonDELETE.Text = "DELETE";
            this.buttonDELETE.UseVisualStyleBackColor = true;
            this.buttonDELETE.Click += new System.EventHandler(this.buttonDELETE_Click);
            // 
            // buttonclose
            // 
            this.buttonclose.Location = new System.Drawing.Point(1475, 6);
            this.buttonclose.Name = "buttonclose";
            this.buttonclose.Size = new System.Drawing.Size(165, 31);
            this.buttonclose.TabIndex = 15;
            this.buttonclose.Text = "CLOSE";
            this.buttonclose.UseVisualStyleBackColor = true;
            this.buttonclose.Click += new System.EventHandler(this.buttonclose_Click);
            // 
            // buttonADD
            // 
            this.buttonADD.Location = new System.Drawing.Point(1475, 154);
            this.buttonADD.Name = "buttonADD";
            this.buttonADD.Size = new System.Drawing.Size(165, 31);
            this.buttonADD.TabIndex = 10;
            this.buttonADD.Text = "ADD";
            this.buttonADD.UseVisualStyleBackColor = true;
            this.buttonADD.Click += new System.EventHandler(this.buttonADD_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(438, 196);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 25);
            this.label10.TabIndex = 1;
            this.label10.Text = "PHOTO";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(438, 160);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(145, 25);
            this.label14.TabIndex = 1;
            this.label14.Text = "AUTO CODE";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(438, 123);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 25);
            this.label8.TabIndex = 1;
            this.label8.Text = "ΕΤΟΣ ΕΩΣ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(438, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(127, 25);
            this.label7.TabIndex = 1;
            this.label7.Text = "ΕΤΟΣ ΑΠΟ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 196);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(175, 25);
            this.label13.TabIndex = 1;
            this.label13.Text = "update_interval";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 237);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(135, 25);
            this.label15.TabIndex = 1;
            this.label15.Text = "ΠΟΣΟΤΗΤΑ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 25);
            this.label6.TabIndex = 1;
            this.label6.Text = "ΜΟΝΤΕΛΟ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 25);
            this.label4.TabIndex = 1;
            this.label4.Text = "ΚΑΤΗΓΟΡΙΑ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 25);
            this.label5.TabIndex = 1;
            this.label5.Text = "ΜΑΡΚΑ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(438, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(144, 25);
            this.label11.TabIndex = 1;
            this.label11.Text = "ΚΑΤΑΣΤΑΣΗ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(438, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 25);
            this.label9.TabIndex = 1;
            this.label9.Text = "ΤΙΜΗ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1096, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 25);
            this.label3.TabIndex = 1;
            this.label3.Text = "ΠΕΡΙΓΡΑΦΗ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "ΤΙΤΛΟΣ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "ΚΩΔΙΚΟΣ";
            // 
            // txtprice
            // 
            this.txtprice.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.πΡΟΙΟΝΤΑBindingSource, "price", true));
            this.txtprice.Location = new System.Drawing.Point(587, 6);
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(116, 31);
            this.txtprice.TabIndex = 3;
            // 
            // txtdes
            // 
            this.txtdes.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.πΡΟΙΟΝΤΑBindingSource, "description", true));
            this.txtdes.Location = new System.Drawing.Point(885, 37);
            this.txtdes.Multiline = true;
            this.txtdes.Name = "txtdes";
            this.txtdes.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtdes.Size = new System.Drawing.Size(565, 154);
            this.txtdes.TabIndex = 9;
            // 
            // txttitle
            // 
            this.txttitle.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.πΡΟΙΟΝΤΑBindingSource, "title", true));
            this.txttitle.Location = new System.Drawing.Point(155, 43);
            this.txttitle.Name = "txttitle";
            this.txttitle.Size = new System.Drawing.Size(277, 31);
            this.txttitle.TabIndex = 1;
            this.txttitle.Leave += new System.EventHandler(this.txttitle_Leave);
            // 
            // txtcode
            // 
            this.txtcode.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.πΡΟΙΟΝΤΑBindingSource, "unique_id", true));
            this.txtcode.Location = new System.Drawing.Point(155, 6);
            this.txtcode.Name = "txtcode";
            this.txtcode.ReadOnly = true;
            this.txtcode.Size = new System.Drawing.Size(277, 31);
            this.txtcode.TabIndex = 0;
            this.txtcode.Click += new System.EventHandler(this.txtcode_Click);
            // 
            // buttonDownload
            // 
            this.buttonDownload.Location = new System.Drawing.Point(626, 94);
            this.buttonDownload.Name = "buttonDownload";
            this.buttonDownload.Size = new System.Drawing.Size(131, 54);
            this.buttonDownload.TabIndex = 28;
            this.buttonDownload.Text = "Download";
            this.buttonDownload.UseVisualStyleBackColor = true;
            this.buttonDownload.Click += new System.EventHandler(this.buttonDownload_Click);
            // 
            // buttonUpload
            // 
            this.buttonUpload.Location = new System.Drawing.Point(367, 94);
            this.buttonUpload.Name = "buttonUpload";
            this.buttonUpload.Size = new System.Drawing.Size(131, 54);
            this.buttonUpload.TabIndex = 28;
            this.buttonUpload.Text = "Upload";
            this.buttonUpload.UseVisualStyleBackColor = true;
            this.buttonUpload.Click += new System.EventHandler(this.buttonUpload_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.tabControl1.Location = new System.Drawing.Point(10, 34);
            this.tabControl1.MaximumSize = new System.Drawing.Size(1920, 1200);
            this.tabControl1.MinimumSize = new System.Drawing.Size(1024, 768);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1654, 1017);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tabPage1.Controls.Add(this.groupBoxDataBase);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1646, 979);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "ΡΥΘΜΙΣΕΙΣ";
            // 
            // groupBoxDataBase
            // 
            this.groupBoxDataBase.BackColor = System.Drawing.Color.DarkGray;
            this.groupBoxDataBase.Controls.Add(this.buttonResetDatabaseBackups);
            this.groupBoxDataBase.Controls.Add(this.buttonUpload);
            this.groupBoxDataBase.Controls.Add(this.comboBoxDatabaseBackups);
            this.groupBoxDataBase.Controls.Add(this.buttonDownload);
            this.groupBoxDataBase.Location = new System.Drawing.Point(48, 36);
            this.groupBoxDataBase.Name = "groupBoxDataBase";
            this.groupBoxDataBase.Size = new System.Drawing.Size(802, 220);
            this.groupBoxDataBase.TabIndex = 30;
            this.groupBoxDataBase.TabStop = false;
            this.groupBoxDataBase.Text = "Data Base";
            // 
            // buttonResetDatabaseBackups
            // 
            this.buttonResetDatabaseBackups.Location = new System.Drawing.Point(36, 94);
            this.buttonResetDatabaseBackups.Name = "buttonResetDatabaseBackups";
            this.buttonResetDatabaseBackups.Size = new System.Drawing.Size(243, 65);
            this.buttonResetDatabaseBackups.TabIndex = 30;
            this.buttonResetDatabaseBackups.Text = "Check for avalilable databses!";
            this.buttonResetDatabaseBackups.UseVisualStyleBackColor = true;
            this.buttonResetDatabaseBackups.Click += new System.EventHandler(this.buttonResetDatabaseBackups_Click);
            // 
            // comboBoxDatabaseBackups
            // 
            this.comboBoxDatabaseBackups.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDatabaseBackups.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.comboBoxDatabaseBackups.FormattingEnabled = true;
            this.comboBoxDatabaseBackups.Location = new System.Drawing.Point(36, 47);
            this.comboBoxDatabaseBackups.Name = "comboBoxDatabaseBackups";
            this.comboBoxDatabaseBackups.Size = new System.Drawing.Size(721, 28);
            this.comboBoxDatabaseBackups.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.Location = new System.Drawing.Point(1672, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "0.1.1.6";
            // 
            // conditionΠΡΟΙΟΝΤΑBindingSource
            // 
            this.conditionΠΡΟΙΟΝΤΑBindingSource.DataMember = "conditionΠΡΟΙΟΝΤΑ";
            this.conditionΠΡΟΙΟΝΤΑBindingSource.DataSource = this.conditionBindingSource;
            // 
            // conditionBindingSource
            // 
            this.conditionBindingSource.DataMember = "condition";
            this.conditionBindingSource.DataSource = this._InventoryProgram_0_1DataSet;
            // 
            // makeBindingSource1
            // 
            this.makeBindingSource1.DataMember = "make";
            this.makeBindingSource1.DataSource = this._InventoryProgram_0_1DataSet;
            // 
            // πΡΟΙΟΝΤΑTableAdapter
            // 
            this.πΡΟΙΟΝΤΑTableAdapter.ClearBeforeFill = true;
            // 
            // makeTableAdapter
            // 
            this.makeTableAdapter.ClearBeforeFill = true;
            // 
            // modelTableAdapter
            // 
            this.modelTableAdapter.ClearBeforeFill = true;
            // 
            // πΡΟΙΟΝΤΑBindingSource1
            // 
            this.πΡΟΙΟΝΤΑBindingSource1.DataMember = "ΠΡΟΙΟΝΤΑ";
            this.πΡΟΙΟΝΤΑBindingSource1.DataSource = this._InventoryProgram_0_1DataSet;
            // 
            // πΡΟΙΟΝΤΑBindingSource2
            // 
            this.πΡΟΙΟΝΤΑBindingSource2.DataMember = "ΠΡΟΙΟΝΤΑ";
            this.πΡΟΙΟΝΤΑBindingSource2.DataSource = this._InventoryProgram_0_1DataSet;
            // 
            // yearsTableAdapter
            // 
            this.yearsTableAdapter.ClearBeforeFill = true;
            // 
            // category_idTableAdapter
            // 
            this.category_idTableAdapter.ClearBeforeFill = true;
            // 
            // conditionTableAdapter
            // 
            this.conditionTableAdapter.ClearBeforeFill = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(1731, 1041);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tabControl1);
            this.MaximumSize = new System.Drawing.Size(1920, 1080);
            this.MinimumSize = new System.Drawing.Size(1024, 768);
            this.Name = "Form1";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.πΡΟΙΟΝΤΑBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._InventoryProgram_0_1DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryidBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conditionBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.makeBindingSource)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBoxDataBase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.conditionΠΡΟΙΟΝΤΑBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conditionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.makeBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.πΡΟΙΟΝΤΑBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.πΡΟΙΟΝΤΑBindingSource2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private _InventoryProgram_0_1DataSet _InventoryProgram_0_1DataSet;
        private System.Windows.Forms.BindingSource πΡΟΙΟΝΤΑBindingSource;
        private _InventoryProgram_0_1DataSetTableAdapters.ΠΡΟΙΟΝΤΑTableAdapter πΡΟΙΟΝΤΑTableAdapter;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPREVIOUS;
        private System.Windows.Forms.Button buttonSAVE;
        private System.Windows.Forms.Button buttonexport;
        private System.Windows.Forms.Button buttonDELETE;
        private System.Windows.Forms.Button buttonclose;
        private System.Windows.Forms.Button buttonADD;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.TextBox txtdes;
        private System.Windows.Forms.TextBox txttitle;
        private System.Windows.Forms.TextBox txtcode;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ComboBox comboBoxMake;
        private System.Windows.Forms.BindingSource makeBindingSource;
        private _InventoryProgram_0_1DataSetTableAdapters.makeTableAdapter makeTableAdapter;
        private System.Windows.Forms.ComboBox comboBoxModel;
        private System.Windows.Forms.BindingSource modelBindingSource;
        private _InventoryProgram_0_1DataSetTableAdapters.modelTableAdapter modelTableAdapter;
        private System.Windows.Forms.BindingSource makeBindingSource1;
        private System.Windows.Forms.ComboBox comboBoxYearTo;
        private System.Windows.Forms.ComboBox comboBoxYearFrom;
        private System.Windows.Forms.BindingSource πΡΟΙΟΝΤΑBindingSource1;
        private System.Windows.Forms.BindingSource πΡΟΙΟΝΤΑBindingSource2;
        private System.Windows.Forms.BindingSource yearsBindingSource;
        private _InventoryProgram_0_1DataSetTableAdapters.yearsTableAdapter yearsTableAdapter;
        private System.Windows.Forms.BindingSource yearsBindingSource1;
        private System.Windows.Forms.BindingSource categoryidBindingSource;
        private _InventoryProgram_0_1DataSetTableAdapters.category_idTableAdapter category_idTableAdapter;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxCondition;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.BindingSource conditionBindingSource;
        private _InventoryProgram_0_1DataSetTableAdapters.conditionTableAdapter conditionTableAdapter;
        private System.Windows.Forms.BindingSource conditionBindingSource1;
        private System.Windows.Forms.BindingSource conditionΠΡΟΙΟΝΤΑBindingSource;
        private System.Windows.Forms.TextBox txtPhoto;
        private System.Windows.Forms.Button buttonAddImage;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox comboBoxCategory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxupdate_interval;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxAutoCode;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridViewTextBoxColumn uniqueidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn makeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn yearfromDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn yeartoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn photoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn conditionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn updateintervalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn κΑΤΗΓΟΡΙΑDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn μΑΡΚΑDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn μΟΝΤΕΛΟDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn πΟΣΟΤΗΤΑDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn weightkgDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn salePriceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoriesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tagsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn autocodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox textBoxQuantity;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button buttonQuantReset;
        private System.Windows.Forms.Button buttonQuantPlus;
        private System.Windows.Forms.Button buttonQuantMinus;
        private System.Windows.Forms.Button buttonPriceMinus;
        private System.Windows.Forms.Button buttonPriceMinus10;
        private System.Windows.Forms.Button buttonPricePlus;
        private System.Windows.Forms.Button buttonPricePlus10;
        private System.Windows.Forms.Button buttonChangeYearToMinusPlus;
        private System.Windows.Forms.Button buttonChangeYearFromMinusPlus;
        private System.Windows.Forms.Button buttonChangeYearToMinus;
        private System.Windows.Forms.Button buttonChangeYearFromMinus;
        private System.Windows.Forms.Button buttonConditionChange;
        private System.Windows.Forms.Button buttonUpdateOne;
        private System.Windows.Forms.Button buttonUpdatePlus;
        private System.Windows.Forms.Button buttonUpdateMinus;
        private System.Windows.Forms.Button buttonDownload;
        private System.Windows.Forms.Button buttonUpload;
        private System.Windows.Forms.ComboBox comboBoxDatabaseBackups;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBoxDataBase;
        private System.Windows.Forms.Button buttonResetDatabaseBackups;
    }
}

