﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace InventoryManagement
{
    public partial class Category : Form
    {
        //Vars:
        DataTable dataTable;
        public string CatValue;
        //EndVars

        public Category()
        {
            InitializeComponent();
        }

        private void Category_Load(object sender, EventArgs e)
        {
            dataTable = _InventoryProgram_0_1DataSet.category_id;
            // TODO: This line of code loads data into the '_InventoryProgram_0_1DataSet.category_id' table. You can move, or remove it, as needed.
            this.category_idTableAdapter.Fill(this._InventoryProgram_0_1DataSet.category_id);
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 2;
        }

        private void LoadData(ComboBox cb, ComboBox cb2)
        {
            DataRowView view = cb.SelectedItem as DataRowView;
            string filter = view["id"].ToString();
            DataRow[] rows = dataTable.Select("parent_id = '" + filter + "'");
            DataTable newTable = dataTable.Clone();
            foreach (DataRow row in rows)
                newTable.ImportRow(row);
            cb2.DataSource = newTable.DefaultView;
            cb2.DisplayMember = "name";
            cb2.ValueMember = "id";
        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedIndex > -1)
            {
                LoadData(comboBox2, comboBox3);
            }
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex > -1)
            {
                LoadData(comboBox1, comboBox2);
            }
        }

        private void comboBox3_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox3.SelectedIndex > -1)
            {
                LoadData(comboBox3, comboBox4);
            }
        }

        private void comboBox4_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox4.SelectedIndex > -1)
            {
                LoadData(comboBox4, comboBox5);
            }
        }

        private void comboBox5_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox5.SelectedIndex > -1)
            {
                LoadData(comboBox5, comboBox6);
            }
        }

        private void comboBox6_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public  void buttonOK_Click(object sender, EventArgs e)
        {
            if (comboBox6.SelectedIndex != -1) { CatValue = comboBox6.SelectedValue.ToString(); }
            else if (comboBox5.SelectedIndex != -1) { CatValue = comboBox5.SelectedValue.ToString(); }
            else if (comboBox4.SelectedIndex != -1) { CatValue = comboBox4.SelectedValue.ToString(); }
            else if (comboBox3.SelectedIndex != -1) { CatValue = comboBox3.SelectedValue.ToString(); }
            else if (comboBox2.SelectedIndex != -1) { CatValue = comboBox2.SelectedValue.ToString(); }
            else if (comboBox1.SelectedIndex != -1) { CatValue = comboBox1.SelectedValue.ToString(); }
            else { CatValue = "error"; }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        public string TheValue
        {
            get { return CatValue; }
        }
    }
}
