﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Xml;
using System.IO;
using System.Net;
using System.Threading;
using System.Globalization;

namespace InventoryManagement
{
    public partial class Form1 : Form
    {
        //Start
        private string nameAddDatabaseBackups = "Database DATE:  ";
        private string filePathsave = null;
        private string CompleteServerPathFiles = "ftp://antallaktika-smart.com/wp-content/uploads/Car_gr/";
        private string Username = "antallaktikasmartcom";
        private string Password = "callerIDphone11}";
        private string FileName, PathFile;
        private int Flag = 3;//1= put to image, 0 = do nothing , 3 = not initialized
        private Action Mymethod;
        private int STA;
        private string Car_grXMLUrl = "C:/Sklavis/Car_grXML.xml";
        private string DatabasePath = "C:/Sklavis/";
        private string tempXMLUrl = "C:/Sklavis/tempXML.xml";
        private string XSLT_Car_grUrl = "C:/Sklavis/XSLT_Car_gr.xslt";
        private string FileName_XML = "Car_grXML.xml";
        private string FileName_XSLT = "XSLT_Car_gr.xslt";
        private string autoCode;
        private string CompleteServerPathDataBase = "ftp://antallaktika-smart.com/wp-content/uploads/Car_gr/DataBase/";
        private string FileNameDataBase = "InventoryProgram_0.1.accdb";
        private string PathDataBase = "C:/Sklavis/InventoryProgram_0.1.accdb";
        private string PathOldDataBase = "C:/Sklavis/OldDatabase/";
        private string NewFileNameDataBase, NewPathDataBase;
        private List<string> Directories = new List<string>();
        public List<string> DirectoriesDatabase = new List<string>();
        public List<string> ReadableDirectoriesDatabaseBackups = new List<string>();
        private string TargetString;
        private string ServerPathSetupXSL = "ftp://antallaktika-smart.com/wp-content/uploads/Car_gr/setup/XSLT_Car_gr.xslt";
        private string CompleteServerPathXSL = "ftp://antallaktika-smart.com/wp-content/uploads/Car_gr/setup/";
        //End
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            new FileInfo(DatabasePath).Directory.Create();
            // TODO: This line of code loads data into the '_InventoryProgram_0_1DataSet.condition' table. You can move, or remove it, as needed.
            this.conditionTableAdapter.Fill(this._InventoryProgram_0_1DataSet.condition);
            // TODO: This line of code loads data into the '_InventoryProgram_0_1DataSet.category_id' table. You can move, or remove it, as needed.
            this.category_idTableAdapter.Fill(this._InventoryProgram_0_1DataSet.category_id);
            // TODO: This line of code loads data into the '_InventoryProgram_0_1DataSet.years' table. You can move, or remove it, as needed.
            this.yearsTableAdapter.Fill(this._InventoryProgram_0_1DataSet.years);
            // TODO: This line of code loads data into the '_InventoryProgram_0_1DataSet.model' table. You can move, or remove it, as needed.
            this.modelTableAdapter.Fill(this._InventoryProgram_0_1DataSet.model);
            // TODO: This line of code loads data into the '_InventoryProgram_0_1DataSet.make' table. You can move, or remove it, as needed.
            this.makeTableAdapter.Fill(this._InventoryProgram_0_1DataSet.make);
            // TODO: This line of code loads data into the '_InventoryProgram_0_1DataSet.ΠΡΟΙΟΝΤΑ' table. You can move, or remove it, as needed.
            this.πΡΟΙΟΝΤΑTableAdapter.Fill(this._InventoryProgram_0_1DataSet.ΠΡΟΙΟΝΤΑ);

        }
        private void buttonADD_Click(object sender, EventArgs e)
        {
            try
            {
                πΡΟΙΟΝΤΑBindingSource.AddNew();
                comboBoxMake.SelectedIndex = 56;
                comboBoxModel.SelectedIndex = 2;
                comboBoxYearFrom.SelectedIndex = 0;
                DateTime date = DateTime.Today;
                int year = date.Year;
                comboBoxYearTo.Text = "" + year;
                comboBoxCondition.SelectedIndex = 0;
                textBoxupdate_interval.Text = "1";
                textBoxQuantity.Text = "1";
                autoCode = generateAutoCode();
                textBoxAutoCode.Text = autoCode;
                txtprice.Text = "0";
            }
            catch
            {
                MessageBox.Show("Finish with this item first.");
            }
        }
        private string generateAutoCode()
        {
            int max = 0;
            int x;
            DataTable tb = _InventoryProgram_0_1DataSet.ΠΡΟΙΟΝΤΑ;
            string tempLast, temp;
            string returnValue = "error";
            foreach (DataRow row in tb.Rows)
            {
                try { temp = "" + row["smart_code"]; } catch { continue; }
                if (temp == textBoxAutoCode.Text) continue;
                try { tempLast = temp.Substring(temp.Length - 4); } catch { continue; }
                //tempFirst = temp.Substring(0, temp.Length - 4);
                x = Int32.Parse(tempLast);
                if (x > max) max = x;
            }
            max++;
            if (max.ToString().Length == 1) { returnValue = "A000" + max; }
            if (max.ToString().Length == 2) { returnValue = "A00" + max; }
            if (max.ToString().Length == 3) { returnValue = "A0" + max; }
            if (max.ToString().Length == 4) { returnValue = "A" + max; }
            return returnValue;
        }
        private void buttonSAVE_Click(object sender, EventArgs e)
        {
            save();

        }
        private void save()
        {
            try
            {
                Validate();
                πΡΟΙΟΝΤΑBindingSource.EndEdit();
                πΡΟΙΟΝΤΑTableAdapter.Update(_InventoryProgram_0_1DataSet.ΠΡΟΙΟΝΤΑ);
                //πΡΟΙΟΝΤΑTableAdapter.Update(_InventoryProgram_0_1DataSet);
                MessageBox.Show("Update successful");
            }
            catch (Exception ex)
            {
                bool model = (comboBoxModel.SelectedIndex != -1);
                bool make = (comboBoxMake.SelectedIndex != -1);
                bool cate = (comboBoxCategory.SelectedIndex != -1);
                bool code = (txtcode.Text != "");
                bool title = (txttitle.Text != "");

                if (!(model & make & cate & code & title))
                {
                    string message = "Empty fileds: ,";
                    if (!model) message = message + " model ,";
                    if (!make) message = message + " make ,";
                    if (!code) message = message + " code ,";
                    if (!title) message = message + " title ,";
                    if (!cate) message = message + " categories ,";
                    MessageBox.Show(message, "ERROR");
                    return;
                }
                MessageBox.Show("A value is invalid.Check again all required fields ? " + "\n" + ex, "ERROR...");
            }
            this.Refresh();
        }
        private string createID()
        {
            string returnValue = "error";
            string makeShort = "make";
            string modelShort = "model";

            switch (comboBoxMake.Text)
            {
                case "Smart":
                    modelShort = "SM";
                    break;
            }
            switch (comboBoxModel.Text)
            {
                case "ForTwo":
                    makeShort = "F2";
                    break;
                case "ForFour":
                    makeShort = "F4";
                    break;
            }
            string instanseString = modelShort + "-" + makeShort + "-" + comboBoxCategory.SelectedValue + "-";
            int max = 0;
            int x;
            DataTable tb = _InventoryProgram_0_1DataSet.ΠΡΟΙΟΝΤΑ;
            string tempLast, temp, tempFirst, temp2;
            foreach (DataRow row in tb.Rows)
            {
                try { temp = "" + row["unique_id"]; } catch { continue; }
                temp2 = "" + row["smart_code"];
                if (temp2 == textBoxAutoCode.Text) continue;
                tempLast = temp.Substring(temp.Length - 3);
                tempFirst = temp.Substring(0, temp.Length - 3);

                if (instanseString == tempFirst)
                {

                    x = Int32.Parse(tempLast);
                    if (x > max) max = x;
                }
            }
            max++;
            if (max.ToString().Length == 1) { returnValue = instanseString + "00" + max; }
            if (max.ToString().Length == 2) { returnValue = instanseString + "0" + max; }
            if (max.ToString().Length == 3) { returnValue = instanseString + max; }
            return returnValue;
        }
        private void buttonNext_Click(object sender, EventArgs e)
        {
            this.πΡΟΙΟΝΤΑBindingSource.MoveNext();
        }
        private void buttonPREVIOUS_Click(object sender, EventArgs e)
        {
            this.πΡΟΙΟΝΤΑBindingSource.MovePrevious();
        }
        private void buttonDELETE_Click(object sender, EventArgs e)
        {

            var result = MessageBox.Show("Are you sure you want to delete this product?", "Delete Product",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                this.πΡΟΙΟΝΤΑBindingSource.RemoveCurrent();
            }
            save();

        }
        private void buttonclose_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Are you sure you would like to exit?", "Closing Program",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        private void buttonexport_Click(object sender, EventArgs e)
        {
            //use Wait
            Mymethod = ButtonExportProsses;
            STA = 1;
            using (Wait wait = new Wait(WaitThreadInitialize))
            {
                wait.ShowDialog(this);
            }
            Flag = 3;
        }
        private void ButtonExportProsses()
        {
            //Declare URL places
            string xmlUrl = Car_grXMLUrl;
            string xmlUrl2 = tempXMLUrl;
            string xsltUrl = XSLT_Car_grUrl;
            string fileName_XML = FileName_XML;
            //Create and transfomr the xml
            _InventoryProgram_0_1DataSet.WriteXml(xmlUrl2, XmlWriteMode.IgnoreSchema);
            var myXslTrans = new XslCompiledTransform();
            myXslTrans.Load(xsltUrl);
            myXslTrans.Transform(xmlUrl2, xmlUrl);
            // Add time
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlUrl);
            string time = DateTime.UtcNow.ToString("o");
            var node = doc.SelectSingleNode("/cardealer/lastupdate").InnerText = time;
            doc.Save(xmlUrl);
            //Upload
            Flag = ConnectAndUpload(fileName_XML, xmlUrl, CompleteServerPathFiles, true);
        }
        private void comboBoxMake_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.modelBindingSource.DataMember = "model";
            this.modelBindingSource.DataSource = this._InventoryProgram_0_1DataSet;
            this.modelBindingSource.Filter = "make_name = '" + comboBoxMake.Text + "'";
        }
        private void buttonAddImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            if (filePathsave == null) fileDialog.InitialDirectory = "c:\\";
            else fileDialog.InitialDirectory = filePathsave;
            fileDialog.RestoreDirectory = false;
            fileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            DialogResult dialogResult = fileDialog.ShowDialog(this);
            if (dialogResult == DialogResult.OK)
            {
                //take results
                string localPathFile = fileDialog.FileName;
                string localFileName = fileDialog.SafeFileName;
                //rename to id
                string pathOnly = localPathFile.Replace(localFileName, "");
                string fileExtension = Path.GetExtension(localFileName);
                PathFile = pathOnly + txtcode.Text + fileExtension;
                FileName = txtcode.Text + fileExtension;
                var result = DialogResult.Yes;
                if (File.Exists(PathFile) & (localPathFile != PathFile))
                {
                    result = MessageBox.Show("A file with the name " + FileName + " already exists. Do you want to overwrite it?", "Delete old file ?",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes) File.Delete(PathFile);
                    else MessageBox.Show("Action declined, nothing happend", "Rollback");
                }
                try
                {

                    if (result == DialogResult.Yes)
                    {
                        if (localPathFile != PathFile) File.Move(localPathFile, PathFile);
                        //use Wait
                        Mymethod = ButtonAddImageProsses;
                        STA = 1;
                        using (Wait wait = new Wait(WaitThreadInitialize))
                        {
                            wait.ShowDialog(this);
                        }
                        if (Flag == 1)
                        {
                            string httpServerPath = CompleteServerPathFiles.Replace("ftp://", "http://");
                            txtPhoto.Text = httpServerPath + FileName;
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("Something went Wrong", "Rollback");
                }
                Flag = 3;
            }
        }
        private void ButtonAddImageProsses()
        {
            string fileNameImage = FileName;
            string pathImage = PathFile;
            Flag = ConnectAndUpload(fileNameImage, pathImage, CompleteServerPathFiles, false);
        }
        private int ConnectAndUpload(string fileName, string filePath, string serverPath, bool message)
        {
            string completeServerPath = serverPath;
            string password = Password;
            string username = Username;
            try
            {
                //Create FTP request
                FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(completeServerPath + fileName);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;
                //Load the file
                FileStream stream = File.OpenRead(@filePath);
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                stream.Close();
                //Upload file
                Stream reqStream = request.GetRequestStream();
                reqStream.Write(buffer, 0, buffer.Length);
                reqStream.Close();
                if (message)MessageBox.Show("Success, The file is now uploaded at:  /n" + completeServerPath + fileName, "Success!");
                return 1;
            }
            catch (WebException re)
            {
                String status = ((FtpWebResponse)re.Response).StatusDescription;
                MessageBox.Show(status, "ERROR");
                return 0;
            }

        }
        private void comboBox2_Click(object sender, EventArgs e)
        {
            using (Category formCat = new Category())
            {
                if (formCat.ShowDialog() == DialogResult.OK)
                {
                    comboBoxCategory.SelectedValue = formCat.TheValue;
                }
            }
        }
        private void txtcode_Click(object sender, EventArgs e)
        {

            bool model = (comboBoxModel.SelectedIndex != -1);
            bool make = (comboBoxMake.SelectedIndex != -1);
            bool cate = (comboBoxCategory.SelectedIndex != -1);
            if (!(model & make & cate))
            {
                string message = "Empty fileds: ,";
                if (!model) message = message + " model ,";
                if (!make) message = message + " make ,";
                if (!cate) message = message + " categories ,";
                MessageBox.Show(message, "ERROR");
                return;
            }
            string value = createID();
            if (txtcode.Text == "") txtcode.Text = value;
            else
            {
                string valueNow = txtcode.Text.Substring(0, txtcode.Text.Length - 3);
                string valuenext = value.Substring(0, value.Length - 3);
                if (valueNow == valuenext) return;
                else txtcode.Text = value;
            }

        }
        private void txttitle_Leave(object sender, EventArgs e)
        {
            string title = txttitle.Text + " - CODE:" + textBoxAutoCode.Text;
            txttitle.Text = title;
            string description;
            string enter = "\r\n";
            description = title + enter + enter + fixData();
            txtdes.Text = description;
        }
        private string fixData()
        {
            string line = "\r\n";
            return "ΣΚΛΑΒΗΣ ΧΡ. ΑΘΑΝΑΣΙΟΣ" + line + line + "ΕΙΣΑΓΩΓΕΣ - ΕΞΑΓΩΓΕΣ ΕΜΠΟΡΙΟ" + line + "ΑΥΤΟΚΙΝΗΤΩΝ - ΑΝΤΑΛΛΑΚΤΙΚΩΝ - ΛΙΠΑΝΤΙΚΩΝ" + line + "ΣΕΠΟΛΙΩΝ 18 ΑΘΗΝΑ 104 45b" + line + "040250382 Α.ΑΘΗΝΩΝ  2108233378";
        }
        private void buttonMinus_Click(object sender, EventArgs e)
        {
            int tempint = Int32.Parse(textBoxQuantity.Text);
            if (tempint != 0) tempint--;
            textBoxQuantity.Text = tempint.ToString();
        }
        private void buttonPlus_Click(object sender, EventArgs e)
        {
            int tempint = Int32.Parse(textBoxQuantity.Text);
            tempint++;
            textBoxQuantity.Text = tempint.ToString();
        }
        private void buttonReset_Click(object sender, EventArgs e)
        {
            int tempint = 0;
            textBoxQuantity.Text = tempint.ToString();
        }
        private void buttonPriceMinus_Click(object sender, EventArgs e)
        {
            int tempint = Int32.Parse(txtprice.Text);
            if (tempint != 0) tempint--;
            txtprice.Text = tempint.ToString();
        }
        private void buttonPricePlus_Click(object sender, EventArgs e)
        {
            int tempint = Int32.Parse(txtprice.Text);
            tempint++;
            txtprice.Text = tempint.ToString();
        }
        private void buttonPriceMinus10_Click(object sender, EventArgs e)
        {
            int tempint = Int32.Parse(txtprice.Text);
            if (tempint >= 10) tempint -= 10;
            txtprice.Text = tempint.ToString();
        }
        private void buttonPricePlus10_Click(object sender, EventArgs e)
        {
            int tempint = Int32.Parse(txtprice.Text);
            tempint += 10;
            txtprice.Text = tempint.ToString();
        }
        private void buttonConditionChange_Click(object sender, EventArgs e)
        {
            if (comboBoxCondition.SelectedIndex == 0) comboBoxCondition.SelectedIndex = 1;
            else if (comboBoxCondition.SelectedIndex == 1) comboBoxCondition.SelectedIndex = 0;
        }
        private void buttonChangeYearFromMinus_Click(object sender, EventArgs e)
        {
            if (comboBoxYearFrom.SelectedIndex == 0) return;
            int temp = comboBoxYearFrom.SelectedIndex;
            temp--;
            comboBoxYearFrom.SelectedIndex = temp;
        }
        private void buttonChangeYearFromMinusPlus_Click(object sender, EventArgs e)
        {
            int temp = comboBoxYearFrom.SelectedIndex;
            temp++;
            try { comboBoxYearFrom.SelectedIndex = temp; } catch { return; }
        }
        private void buttonChangeYearToMinus_Click(object sender, EventArgs e)
        {
            if (comboBoxYearTo.SelectedIndex == 0) return;
            int temp = comboBoxYearTo.SelectedIndex;
            temp--;
            comboBoxYearTo.SelectedIndex = temp;
        }
        private void buttonChangeYearToMinusPlus_Click(object sender, EventArgs e)
        {
            int temp = comboBoxYearTo.SelectedIndex;
            temp++;
            try { comboBoxYearTo.SelectedIndex = temp; } catch { return; }
        }
        private void buttonUpdateMinus_Click(object sender, EventArgs e)
        {
            int tempint = Int32.Parse(textBoxupdate_interval.Text);
            if (tempint != 0) tempint--;
            textBoxupdate_interval.Text = tempint.ToString();
        }
        private void buttonUpdatePlus_Click(object sender, EventArgs e)
        {
            int tempint = Int32.Parse(textBoxupdate_interval.Text);
            tempint++;
            textBoxupdate_interval.Text = tempint.ToString();
        }
        private void buttonUpdateOne_Click(object sender, EventArgs e)
        {
            int tempint = 1;
            textBoxupdate_interval.Text = tempint.ToString();
        }
        private void buttonUpload_Click(object sender, EventArgs e)
        {
            string fileNameDataBase = FileNameDataBase;
            string pathDataBase = PathDataBase.Replace(FileNameDataBase, "");
            string pathOldDataBase = PathOldDataBase;
            string extension = fileNameDataBase.Substring(fileNameDataBase.Length - 6);
            string fileNameOriginal = fileNameDataBase.Substring(0, fileNameDataBase.Length - 6);
            string ft2 = DateTime.Now.ToString("dd-MM-yyyy_hh$mm$ss");
            string OldPathAndFile = pathDataBase + fileNameDataBase;
            string NewPathAndFile = pathOldDataBase + fileNameOriginal + ft2 + extension;
            new FileInfo(pathOldDataBase).Directory.Create();
            File.Copy(OldPathAndFile, NewPathAndFile);
            NewFileNameDataBase = fileNameOriginal + ft2 + extension;
            NewPathDataBase = NewPathAndFile;
            // Upload            
            Mymethod = buttonUploadProsses;
            STA = 1;
            using (Wait wait = new Wait(WaitThreadInitialize))
            {
                wait.ShowDialog(this);
            }
            Flag = 3;
            ResetDatabaseBackupsAction();
        }
        private void buttonUploadProsses()
        {
            string fileName = NewFileNameDataBase;
            string path = NewPathDataBase;
            Flag = ConnectAndUpload(fileName, path, CompleteServerPathDataBase, false);
        }     
        private void buttonResetDatabaseBackups_Click(object sender, EventArgs e)
        {

            ResetDatabaseBackupsAction();
        }

        private void ResetDatabaseBackupsAction()
        {

            Mymethod = buttonResetDatabaseBackupsProsses;
            STA = 1;
            using (Wait wait = new Wait(WaitThreadInitialize))
            {
                wait.ShowDialog(this);
            }
            ;
            BindingSource bs = new BindingSource();
            List<string> tempList = new List<string>(ReadableDirectoriesDatabaseBackups);
            tempList.Reverse();
            bs.DataSource = tempList;
            comboBoxDatabaseBackups.DataSource = bs;
            Flag = 3;
        }

        private void makeDatabaseBackupsReadable()
        {
            ReadableDirectoriesDatabaseBackups.Clear();
            string temp,normalName;
            foreach (string Txt in DirectoriesDatabase)
            {
                temp = Txt.Substring(Txt.Length - 25);
                temp = temp.Substring(0, temp.Length - 6);
                temp = temp.Replace("_"," ");
                temp = temp.Replace("$", ":");
                normalName = nameAddDatabaseBackups + temp;
                ReadableDirectoriesDatabaseBackups.Add(normalName);
            }
        }
        private void buttonResetDatabaseBackupsProsses()
        {
            DirectoriesDatabase.Clear();
            string remoteFTPPath = CompleteServerPathDataBase;
            var request = (FtpWebRequest)WebRequest.Create(remoteFTPPath);
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(Username, Password);
            request.Proxy = null;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);

            string line = reader.ReadLine();

            while (!string.IsNullOrEmpty(line))
            {
                DirectoriesDatabase.Add(line);
                line = reader.ReadLine();
            }
            reader.Close();
            response.Close();
            makeDatabaseBackupsReadable();
            Flag = 1;
        }
        private void buttonDownload_Click(object sender, EventArgs e)
        {
            string targetValue = comboBoxDatabaseBackups.Text.Replace(nameAddDatabaseBackups, "");
            targetValue = targetValue.Replace(" ", "_");
            targetValue = targetValue.Replace(":", "$");

            string tempValue;
            string target = "null";
            foreach (string Txt in DirectoriesDatabase)
            {
                tempValue = Txt.Substring(Txt.Length - 25);
                tempValue = tempValue.Substring(0, tempValue.Length - 6);
                if (tempValue == targetValue) { target = Txt; break; }
            }
            TargetString = target;
            Mymethod = buttonDownloadProsses;
            STA = 1;
            using (Wait wait = new Wait(WaitThreadInitialize))
            {
                wait.ShowDialog(this);
            }
            ;           
            Flag = 3;
            Application.Restart();
        }
        private void ConnectAndDownload(string serverFileName,string localFileName , string serverPath , string LocalPath)
        {
            //
            //MessageBox.Show("serverFileName=  " + serverFileName);
            //MessageBox.Show("localFileName=  " + localFileName);
           // MessageBox.Show("serverPath=  " + serverPath);
            string totalPath = LocalPath + localFileName;
            new FileInfo(totalPath).Directory.Create();
            File.Delete(totalPath);
            
            string password = Password;
            string username = Username;
            string serverPathAndFilename = serverPath + serverFileName;
            string localFilePath = LocalPath + localFileName;

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(serverPathAndFilename);
            request.Method = WebRequestMethods.Ftp.DownloadFile;            
            request.Credentials = new NetworkCredential(username, password);
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            using (var file = File.Create(localFilePath))
            {

                responseStream.CopyTo(file);
            }
            reader.Close();
            response.Close();
            
        }
        private void buttonDownloadProsses()
        {
            string localFileName= TargetString;


            string extension = localFileName.Substring(localFileName.Length - 6);
            string fileNameOriginal = localFileName.Substring(0, 20);
            localFileName = fileNameOriginal + extension;
            new FileInfo(localFileName).Directory.Create();
            File.Delete(localFileName);

            ConnectAndDownload(TargetString , localFileName, CompleteServerPathDataBase, DatabasePath);
            ConnectAndDownload(FileName_XSLT, FileName_XSLT, CompleteServerPathXSL, DatabasePath);
            Flag = 1;
        }
        private void WaitThreadInitialize()
        {
            Action myMethod = Mymethod;
            int sta = STA;
            Thread thread = new Thread(new ThreadStart(myMethod));
            if (sta == 1) thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            SpinWait.SpinUntil(condition: () => (Flag < 3));
        }
    }
}
